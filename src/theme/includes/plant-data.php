<?php

add_filter('woocommerce_product_tabs', 'pq_plant_woo_new_product_tabs');
function pq_plant_woo_new_product_tabs($tabs){
    // Adds the new tab
    $plant_data_active = get_post_meta(get_the_ID(), 'plant-data-active', true);
    if ($plant_data_active != 'on') return $tabs;
    $tabs['reviews']['priority'] = 30;
    $new_tabs = array(
        'plant_care' => array(
            'title'     => __('Información General', 'woocommerce'),
            'priority'  => 10,
            'callback'  => 'pq_plant_general_info_content'
        ),
        'plant_additional_information' => array(
            'title'     => __('Información Adicional', 'woocommerce'),
            'priority'  => 20,
            'callback'  => 'pq_plant_additional_information_content'
        ),
        $tabs['reviews']
    );
    return $new_tabs;
}

function pq_plant_additional_information_content(){
    // The new tab content
    $post_id = get_the_ID();
    $considerations = get_post_meta($post_id, 'plant-data-considerations', true);
    $faq = get_post_meta($post_id, 'plant-data-faq', true);
?>
    <div><?php the_content(); ?></div>
    <?php if ($considerations) : ?>
        <table class="woocommerce-product-attributes shop_attributes">
            <tbody>
                <tr class="woocommerce-product-attributes-item">
                    <th class="woocommerce-product-attributes-item__label">
                        <h3><strong>Consideraciones</strong></h3>
                    </th>
                </tr>
                <tr class="woocommerce-product-attributes-item">
                    <td class="woocommerce-product-attributes-item__value">
                        <?php echo $considerations; ?>
                    </td>
                </tr>
            </tbody>
        </table><br>
    <?php endif; ?>
    <?php if ($faq) : ?>
        <table class="woocommerce-product-attributes shop_attributes">
            <tbody>
                <tr class="woocommerce-product-attributes-item">
                    <th class="woocommerce-product-attributes-item__label">
                        <h3><strong>Solución a dudas comunes</strong></h3>
                    </th>
                </tr>
                <tr class="woocommerce-product-attributes-item">
                    <td class="woocommerce-product-attributes-item__value">
                        <?php echo $faq; ?>
                    </td>
                </tr>
            </tbody>
        </table><br>
    <?php endif; ?>
    <?php woocommerce_product_additional_information_tab(); ?>
<?php
}
function pq_plant_general_info_content(){
    // The new tab content
    $post_id = get_the_ID();
    $simbology = get_post_meta($post_id, 'plant-data-simbology', true);
    $sun = get_post_meta($post_id, 'plant-data-sun', true);
    $water = get_post_meta($post_id, 'plant-data-water', true);
    $humidity = get_post_meta($post_id, 'plant-data-humidity', true);
    $additional_care = get_post_meta($post_id, 'plant-data-additional-care', true);
?>
    <style>
        .plant-data-icon {
            width: 30px;
            display: inline-block;
            margin: 5px;
        }
    </style>
    <?php if ($simbology) : ?>
        <table class="woocommerce-product-attributes shop_attributes">
            <tbody>
                <tr class="woocommerce-product-attributes-item">
                    <th class="woocommerce-product-attributes-item__label">
                        <h3><strong>Simbología</strong></h3>
                    </th>
                </tr>
                <tr class="woocommerce-product-attributes-item">
                    <td class="woocommerce-product-attributes-item__value">
                        <?php echo $simbology; ?>
                    </td>
                </tr>
            </tbody>
        </table><br>
    <?php endif; ?>
    <?php if ($sun) : ?>
        <table class="woocommerce-product-attributes shop_attributes">
            <tbody>
                <tr class="woocommerce-product-attributes-item">
                    <th class="woocommerce-product-attributes-item__label" colspan="2">
                        <h3><strong>Luz</strong></h3>
                    </th>
                </tr>
                <tr class="woocommerce-product-attributes-item">
                    <td class="woocommerce-product-attributes-item__value" style="width: 300px">
                        <?php
                        $sun_level = pq_get_plant_data_sun_level($sun);

                        for ($i = 0; $i < $sun_level; $i++) :
                        ?>
                            <img class="plant-data-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sun_icon.svg" alt="sun icon" title="<?php echo $sun; ?>">
                        <?php endfor; ?>
                    </td>
                    <td class="woocommerce-product-attributes-item__value">
                        <?php echo $sun; ?>
                    </td>
                </tr>
            </tbody>
        </table><br>
    <?php endif; ?>
    <?php if ($water) : ?>
        <table class="woocommerce-product-attributes shop_attributes">
            <tbody>
                <tr class="woocommerce-product-attributes-item">
                    <th class="woocommerce-product-attributes-item__label" colspan="2">
                        <h3><strong>Agua</strong></h3>
                    </th>
                </tr>
                <tr class="woocommerce-product-attributes-item">
                    <td class="woocommerce-product-attributes-item__value" style="width: 300px">
                        <?php
                        $water_level = pq_get_plant_data_water_level($water);
                        for ($i = 0; $i < $water_level; $i++) :
                        ?>
                            <img class="plant-data-icon tooltip" src="<?php echo get_stylesheet_directory_uri(); ?>/img/water_icon.svg" alt="water icon" title="<?php echo $water; ?>">
                        <?php endfor; ?>
                    </td>
                    <td class="woocommerce-product-attributes-item__value">
                        <?php echo $water; ?>
                    </td>
                </tr>
            </tbody>
        </table><br>
    <?php endif; ?>
    <?php if ($humidity) : ?>
        <table class="woocommerce-product-attributes shop_attributes">
            <tbody>
                <tr class="woocommerce-product-attributes-item">
                    <th class="woocommerce-product-attributes-item__label" colspan="2">
                        <h3><strong>Humedad</strong></h3>
                    </th>
                </tr>
                <tr class="woocommerce-product-attributes-item">
                    <td class="woocommerce-product-attributes-item__value">
                        <?php echo $humidity; ?>
                    </td>
                </tr>
            </tbody>
        </table><br>
    <?php endif; ?>
    <?php if ($additional_care) : ?>
        <table class="woocommerce-product-attributes shop_attributes">
            <tbody>
                <tr class="woocommerce-product-attributes-item">
                    <th class="woocommerce-product-attributes-item__label" colspan="2">
                        <h3><strong>Cuidados adicionales</strong></h3>
                    </th>
                </tr>
                <tr class="woocommerce-product-attributes-item">
                    <td class="woocommerce-product-attributes-item__value">
                        <?php echo $additional_care; ?>
                    </td>
                </tr>
            </tbody>
        </table>
    <?php endif; ?>
<?php
}
add_action('wp_ajax_get-sun-level', 'pq_ajax_plant_data_sun_level');
function pq_ajax_plant_data_sun_level(){
    echo pq_get_plant_data_sun_level($_POST['value']);
    wp_die();
}
function pq_get_plant_data_sun_level($value){
    switch ($value) {
        case 'Sombra':
            return 1;
            break;
        case 'Media sombra':
            return 2;
            break;
        case 'Resolana baja a media':
            return 3;
            break;
        case 'Resolana media a alta':
            return 4;
            break;
        case 'Resolana alta a sol directo':
            return 5;
            break;
        default:
            return 0;
            break;
    }
}
add_action('wp_ajax_get-water-level', 'pq_ajax_plant_data_water_level');
// add_action( 'wp_ajax_nopriv_get-water-level', 'pq_ajax_plant_data_water_level' );
function pq_ajax_plant_data_water_level(){
    echo pq_get_plant_data_water_level($_POST['value']);
    wp_die();
}
function pq_get_plant_data_water_level($value){
    switch ($value) {
        case '2 a 3 veces a la semana':
            return 3;
            break;
        case '1 a 2 veces a la semana':
            return 2;
            break;
        case '1 vez cada 10-15 días':
            return 1;
            break;
        default:
            return 0;
            break;
    }
}

/*
 * Add a meta box
 */
add_action('admin_menu', 'wc_upload_meta_box_add');

function wc_upload_meta_box_add(){
    add_meta_box(
        'wcplantdatadiv', // meta box ID
        'Detalles de PlantaQi', // meta box title
        'wc_plant_data_print_box', // callback function that prints the meta box HTML 
        'product', // post type where to add it
        'normal', // priority
        'high'
    ); // position
}

/*
  * Meta Box HTML
  */
function wc_plant_data_print_box($post){
    wp_nonce_field(basename(__FILE__), 'plant-data-nonce');
    $plant_data_active = get_post_meta($post->ID, 'plant-data-active', true);
    $plant_data_simbology = get_post_meta($post->ID, 'plant-data-simbology', true);
    $plant_data_considerations = get_post_meta($post->ID, 'plant-data-considerations', true);
    $plant_data_faq = get_post_meta($post->ID, 'plant-data-faq', true);
    $plant_data_sun = get_post_meta($post->ID, 'plant-data-sun', true);
    $plant_data_water = get_post_meta($post->ID, 'plant-data-water', true);
    $plant_data_humidity = get_post_meta($post->ID, 'plant-data-humidity', true);
    $plant_data_additional_care = get_post_meta($post->ID, 'plant-data-additional-care', true);
    ?>
    <style>
        .plant-data-icon {
            width: 30px;
            display: inline-block;
            margin: 5px;
        }
    </style>
    <table>
        <tbody class="form-table">
            <tr>
                <th style="font-weight:normal">
                    <label for="plant-data-active"><strong><?php _e('Activar datos de planta', 'flatsome-child') ?></strong></label><br>
                    <small>Cambia las pestañas por las de cuidados</small>
                </th>
                <td>
                    <input name="plant-data-active" type="checkbox" id="plant-data-active" <?php echo $plant_data_active == 'on' ? 'checked="checked"' : '' ?> />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <h1>Información adicional</h1>
                </td>
            </tr>
            <tr>
                <th style="font-weight:normal">
                    <label for="plant-data-simbology"><strong><?php _e('Simbología', 'flatsome-child') ?></strong></label>
                </th>
                <td>
                    <?php wp_editor($plant_data_simbology, 'plant-data-simbology', array('textarea_rows' => 4)); ?>
                </td>
            </tr>
            <tr>
                <th style="font-weight:normal">
                    <label for="plant-data-considerations"><strong><?php _e('Consideraciones', 'flatsome-child') ?></strong></label>
                </th>
                <td>
                    <?php wp_editor($plant_data_considerations, 'plant-data-considerations', array('textarea_rows' => 4)); ?>
                </td>
            </tr>
            <tr>
                <th style="font-weight:normal">
                    <label for="plant-data-faq"><strong><?php _e('Dudas comunes', 'flatsome-child') ?></strong></label>
                </th>
                <td>
                    <?php wp_editor($plant_data_faq, 'plant-data-faq', array('textarea_rows' => 4)); ?>

                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <h1>Cuidados</h1>
                </td>
            </tr>
            <tr>
                <th style="font-weight:normal">
                    <label for="plant-data-sun"><strong><?php _e('Sol', 'flatsome-child') ?></strong></label>
                </th>
                <td>
                    <select name="plant-data-sun" id="plant-data-sun">
                        <option value="0">--Nivel--</option>
                        <option <?php echo $plant_data_sun == 'Sombra' ? 'selected' : '';  ?> value="Sombra">Sombra</option>
                        <option <?php echo $plant_data_sun == 'Media sombra' ? 'selected' : '';  ?> value="Media sombra">Media sombra</option>
                        <option <?php echo $plant_data_sun == 'Resolana baja a media' ? 'selected' : '';  ?> value="Resolana baja a media">Resolana baja a media</option>
                        <option <?php echo $plant_data_sun == 'Resolana media a alta' ? 'selected' : '';  ?> value="Resolana media a alta">Resolana media a alta</option>
                        <option <?php echo $plant_data_sun == 'Resolana alta a sol directo' ? 'selected' : '';  ?> value="Resolana alta a sol directo">Resolana alta a sol directo</option>
                    </select>
                    <div id="plant-data-sun-icons"></div>
                </td>
            </tr>
            <tr>
                <th style="font-weight:normal">
                    <label for="plant-data-water"><strong><?php _e('Agua', 'flatsome-child') ?></strong></label>
                </th>
                <td>
                    <select name="plant-data-water" id="plant-data-water">
                        <option value="0">--Nivel--</option>
                        <option <?php echo $plant_data_water == '2 a 3 veces a la semana' ? 'selected' : '' ?> value="2 a 3 veces a la semana">2 a 3 veces a la semana</option>
                        <option <?php echo $plant_data_water == '1 a 2 veces a la semana' ? 'selected' : '' ?> value="1 a 2 veces a la semana">1 a 2 veces a la semana</option>
                        <option <?php echo $plant_data_water == '1 vez cada 10-15 días' ? 'selected' : '' ?> value="1 vez cada 10-15 días">1 vez cada 10-15 días</option>
                    </select>
                    <div id="plant-data-water-icons"></div>
                </td>
            </tr>
            <tr>
                <th style="font-weight:normal">
                    <label for="plant-data-humidity"><strong><?php _e('Humedad', 'flatsome-child') ?></strong>
                </th>
                <td>
                    <?php wp_editor($plant_data_humidity, 'plant-data-humidity', array('textarea_rows' => 4)); ?>
                </td>
            </tr>
            <tr>
                <th style="font-weight:normal">
                    <label for="plant-data-additional-care"><strong><?php _e('Cuidados adicionales', 'flatsome-child') ?></strong></label>
                </th>
                <td>
                    <?php wp_editor($plant_data_additional_care, 'plant-data-additional-care', array('textarea_rows' => 4)); ?>
                </td>
            </tr>
        </tbody>
    </table>
    <script>
        (function($) {
            $(window).load(function() {
                $water_select = $('#plant-data-water');
                $sun_select = $('#plant-data-sun');
                $water_icons = $('#plant-data-water-icons');
                $sun_icons = $('#plant-data-sun-icons');

                load_water_level($water_select.val());
                load_sun_level($sun_select.val());

                $water_select.on('change', function() {
                    load_water_level($(this).val());
                });
                $sun_select.on('change', function() {
                    load_sun_level($(this).val());
                });

                function load_water_level(value) {
                    $.ajax({
                        type: "post",
                        url: '<?php echo admin_url('admin-ajax.php'); ?>',
                        data: {
                            value: value,
                            action: 'get-water-level',
                        },
                        success: function(result) {
                            $water_icons.empty();
                            if (result) {
                                for (var i = 0; i < parseInt(result); i++) {
                                    $water_icons.append('<img class="plant-data-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/img/water_icon.svg" >');
                                }
                            }
                        }
                    });
                }

                function load_sun_level(value) {
                    $.ajax({
                        type: "post",
                        url: '<?php echo admin_url('admin-ajax.php'); ?>',
                        data: {
                            value: value,
                            action: 'get-sun-level',
                        },
                        success: function(result) {
                            $sun_icons.empty();
                            if (result) {
                                for (var i = 0; i < parseInt(result); i++) {
                                    $sun_icons.append('<img class="plant-data-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sun_icon.svg" >');
                                }
                            }
                        }
                    });
                }
            });
        })(jQuery);
    </script>
<?php
}

/*
  * Save Meta Box data
  */
add_action('save_post', 'wc_plant_data_save');

function wc_plant_data_save($post_id){
    // verify nonce
    if (!wp_verify_nonce($_POST['plant-data-nonce'], basename(__FILE__)))
        return $post_id;

    // check autosave
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
        return $post_id;

    // check permissions
    if ('page' == $_POST['post_type']) {
        if (!current_user_can('edit_page', $post_id))
            return $post_id;
    } elseif (!current_user_can('edit_post', $post_id)) {
        return $post_id;
    }

    // Save
    $plant_data_active = $_POST['plant-data-active'];
    update_post_meta($post_id, 'plant-data-active', $plant_data_active);

    $plant_data_simbology = $_POST['plant-data-simbology'];
    update_post_meta($post_id, 'plant-data-simbology', $plant_data_simbology);

    $plant_data_considerations = $_POST['plant-data-considerations'];
    update_post_meta($post_id, 'plant-data-considerations', $plant_data_considerations);

    $plant_data_faq = $_POST['plant-data-faq'];
    update_post_meta($post_id, 'plant-data-faq', $plant_data_faq);

    $plant_data_sun = $_POST['plant-data-sun'];
    update_post_meta($post_id, 'plant-data-sun', $plant_data_sun);

    $plant_data_water = $_POST['plant-data-water'];
    update_post_meta($post_id, 'plant-data-water', $plant_data_water);

    $plant_data_humidity = $_POST['plant-data-humidity'];
    update_post_meta($post_id, 'plant-data-humidity', $plant_data_humidity);

    $plant_data_additional_care = $_POST['plant-data-additional-care'];
    update_post_meta($post_id, 'plant-data-additional-care', $plant_data_additional_care);
}


add_shortcode('plant_data_info_table', 'pq_plant_data_info_table');

function pq_plant_data_info_table(){
    $plant_data_active = get_post_meta(get_the_ID(), 'plant-data-active', true);
    if ($plant_data_active != 'on'):
    ?>
    <style>.medida-maceta{display:none}</style>
    <?php
    else:
    ?>
    <table class="woocommerce-product-attributes shop_attributes">
        <tbody>
            <tr class="woocommerce-product-attributes-item">
                <th class="woocommerce-product-attributes-item__label" colspan="2">
                    <h3><strong>Luz</strong></h3>
                </th>
            </tr>
            <tr class="woocommerce-product-attributes-item">
                <th class="woocommerce-product-attributes-item__label" style="width: 300px;">
                    <img class="plant-data-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sun_icon.svg">
                </th>
                <td class="woocommerce-product-attributes-item__value">
                    Sombra
                </td>
            </tr>
            <tr class="woocommerce-product-attributes-item">
                <th class="woocommerce-product-attributes-item__label">
                    <img class="plant-data-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sun_icon.svg">
                    <img class="plant-data-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sun_icon.svg">
                </th>
                <td class="woocommerce-product-attributes-item__value">
                    Media sombra
                </td>
            </tr>
            <tr class="woocommerce-product-attributes-item">
                <th class="woocommerce-product-attributes-item__label">
                    <img class="plant-data-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sun_icon.svg">
                    <img class="plant-data-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sun_icon.svg">
                    <img class="plant-data-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sun_icon.svg">
                </th>
                <td class="woocommerce-product-attributes-item__value">
                    Resolana baja a media
                </td>
            </tr>
            <tr class="woocommerce-product-attributes-item">
                <th class="woocommerce-product-attributes-item__label">
                    <img class="plant-data-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sun_icon.svg">
                    <img class="plant-data-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sun_icon.svg">
                    <img class="plant-data-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sun_icon.svg">
                    <img class="plant-data-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sun_icon.svg">
                </th>
                <td class="woocommerce-product-attributes-item__value">
                    Resolana media a alta
                </td>
            </tr>
            <tr class="woocommerce-product-attributes-item">
                <th class="woocommerce-product-attributes-item__label">
                    <img class="plant-data-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sun_icon.svg">
                    <img class="plant-data-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sun_icon.svg">
                    <img class="plant-data-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sun_icon.svg">
                    <img class="plant-data-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sun_icon.svg">
                    <img class="plant-data-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sun_icon.svg">
                </th>
                <td class="woocommerce-product-attributes-item__value">
                    Resolana alta a sol directo
                </td>
            </tr>
        </tbody>
    </table>
    <table class="woocommerce-product-attributes shop_attributes" style="margin-bottom: 3em">
        <tbody>
            <tr class="woocommerce-product-attributes-item">
                <th class="woocommerce-product-attributes-item__label" colspan="2">
                    <h3><strong>Agua</strong></h3>
                </th>
            </tr>
            <tr class="woocommerce-product-attributes-item">
                <th class="woocommerce-product-attributes-item__label" style="width: 300px;">
                    <img class="plant-data-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/img/water_icon.svg">
                    <img class="plant-data-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/img/water_icon.svg">
                    <img class="plant-data-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/img/water_icon.svg">
                </th>
                <td class="woocommerce-product-attributes-item__value">
                    2 a 3 veces a la semana
                </td>
            </tr>
            <tr class="woocommerce-product-attributes-item">
                <th class="woocommerce-product-attributes-item__label">
                    <img class="plant-data-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/img/water_icon.svg">
                    <img class="plant-data-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/img/water_icon.svg">
                </th>
                <td class="woocommerce-product-attributes-item__value">
                    1 a 2 veces a la semana
                </td>
            </tr>
            <tr class="woocommerce-product-attributes-item">
                <th class="woocommerce-product-attributes-item__label">
                    <img class="plant-data-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/img/water_icon.svg">
                </th>
                <td class="woocommerce-product-attributes-item__value">
                    1 vez cada 10-15 días
                </td>
            </tr>
        </tbody>
    </table>
    <?php
    endif;
}
