<?php
//Google analytics tag
add_action( 'wp_head', 'pq_google_analytics', 7 );
function pq_google_analytics() {
    ?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-175787461-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-175787461-1');
    </script>
    <?php
}

/**
 * Funciones avanzadas de google analytics para ecommerce
 * 
 */

//Transacción completa
add_action('woocommerce_thankyou', 'pq_ga_add_transaction', 10, 1);
function pq_ga_add_transaction($order_id){
  $order = wc_get_order( $order_id );

  if ( ! $order || get_post_meta($order->get_id(), '_ga_tracked', true) == 1) {
    return;
  }
  $items = array();
  if ( $order->get_items() ) {
    foreach ( $order->get_items() as $item ) {
      $items []= pq_ga_add_item( $item );
    }
  }

  $ga_code = "gtag( 'event', 'purchase',  {";
  $ga_code .= "'transaction_id': '" . esc_js( $order->get_id() ) . "',";
  $ga_code .= "'value': '" . esc_js( $order->get_total() ) . "',";
  $ga_code .= "'currency': '" . esc_js( $order->get_currency() ) . "',";
  $ga_code .= "'items':" . json_encode($items);
  $ga_code .= "} );";


  // Mark the order as tracked.
  update_post_meta( $order_id, '_ga_tracked', 1 );
  echo "<script>".$ga_code."</script>";
}

function pq_ga_add_item($item){
  $product = $item->get_product();
  $categories = array();
  $terms =  get_the_terms($item->get_product_id(), 'product_cat');
  foreach($terms as $term){
    $categories []= $term->name;
  }

  $_item = array(
    'id' => $product->get_sku() ? $product->get_sku() : ( '#' . $item->get_product_id() ),
    'name' => $item->get_name(),
    'brand' => implode(', ', $categories),
    'category' => $item->get_meta('tamano-de-maceta', true),
    'variant' => $item->get_meta('color-de-maceta', true),
    'quantity' =>  $item->get_quantity(),
    'price' => $item->get_total()
  );

  return $_item;
}


// //Agregar al carrito
add_action( 'woocommerce_after_add_to_cart_button', 'pq_ga_add_to_cart');
function pq_ga_add_to_cart(){
  global $product;
  $categories = array();
  $terms =  get_the_terms($product->get_id(), 'product_cat');
  $normal_price = $product->get_price();
  foreach($terms as $term){
    $categories []= $term->name;
  }
  $ga_code = "gtag( 'event', 'add_to_cart', {";
  $ga_code .= "'items': [{";
  $ga_code .= "'id': '" . esc_js( $product->get_sku() ? $product->get_sku() : ( '#' . $product->get_id() ) ) . "',";
  $ga_code .= "'name': '" . esc_js( $product->get_title() ) . "',";
  $ga_code .= "'brand': '" . esc_js( implode(', ', $categories) ) . "',";
  $ga_code .= "'category': $( 'select#color-de-maceta' ).length ? $( 'select#color-de-maceta' ).val() : '' ".",";
  $ga_code .= "'variant': $( 'select#tamano-de-maceta' ).length ? $( 'select#tamano-de-maceta' ).val() : '' ".",";
  $ga_code .= "'price': $( '.woocommerce-variation-price' ).length && $( '.woocommerce-variation-price' ).text().replace('$', '') ? $( '.woocommerce-variation-price' ).text().replace('$', '') : '".$normal_price."' ".",";
  $ga_code .= "'quantity': $( 'input.qty' ).length ? $( 'input.qty' ).val() : '1'".",";
  $ga_code .= "} ] } );";

  $selector = '.single_add_to_cart_button';
  pq_ga_set_add_to_cart_code($selector, $ga_code);
}


// add_action( 'wp_footer', 'pq_ga_add_to_cart_loop' );
// function pq_ga_add_to_cart_loop(){
//   $ga_code = "ga( 'ec:addProduct', {";
//   $ga_code .= "'id': ($(this).data('product_sku')) ? ($(this).data('product_sku')) : ('#' + $(this).data('product_id')),";
//   $ga_code .= "'quantity': $(this).data('quantity')";
//   $ga_code .= "} );";
  
//   $selector = '.add_to_cart_button:not(.product_type_variable, .product_type_grouped)';
//   pq_ga_set_add_to_cart_code($selector, $ga_code);

// }

function pq_ga_set_add_to_cart_code($selector, $ga_code){
  ?>
  <script>
    (function($) {
      $( document.body ).on( 'click', '<?php echo $selector ?>', function(e) {
        <?php echo $ga_code ?>
      });
    })(jQuery);
  </script>
  <?php
}

// //Quitar del carrito
add_action( 'woocommerce_after_cart', 'pq_ga_remove_from_cart' );
add_action( 'woocommerce_after_mini_cart', 'pq_ga_remove_from_cart' );
function pq_ga_remove_from_cart(){
  global $woocommerce;
  $items = $woocommerce->cart->get_cart();
  $cart = array();
  foreach($items as $item){
    $item_data = $item['data'];
    
    $categories = array();
    $terms =  get_the_terms($item['product_id'], 'product_cat');
    foreach($terms as $term){
      $categories []= $term->name;
    }
    $_item = array(
      'id' => $item_data->get_sku() ? $item_data->get_sku() : ( '#' . $item['id'] ),
      'name' => $item_data->get_name(),
      'brand' => implode(', ', $categories),
      'category' => $item['variation']['attribute_tamano-de-maceta'] ? $item['variation']['attribute_tamano-de-maceta'] : '',
      'variant' => $item['variation']['attribute_color-de-maceta'] ? $item['variation']['attribute_color-de-maceta'] : '',
      'quantity' =>  $item['quantity'],
      'price' => $item['line_total']
    );
    $cart[$item['product_id']] = $_item;
  }
  ?>
    <script>
    (function($) {
      cart_items = JSON.parse('<?php echo json_encode($cart); ?>');
      removed_items = [];
      $( document.body ).on( 'click', '.remove', function() {
        id = $(this).data('product_id');
        if(removed_items.includes(id)) return;
        console.log('remove');
        gtag( 'event', 'remove_from_cart', {
          'id' : cart_items[id]['id'],
          'name' : cart_items[id]['name'],
          'brand' : cart_items[id]['brand'],
          'category' : cart_items[id]['category'],
          'variant' : cart_items[id]['variant'],
          'quantity' : cart_items[id]['quantity'],
          'price' : cart_items[id]['price']
        } );
        removed_items.push(id);
      });
    })(jQuery);
    </script>
 <?php
}

/**
 * begin_checkout
 */

add_action( 'woocommerce_after_cart', 'pq_ga_begin_checkout' );
function pq_ga_begin_checkout(){
  pq_set_checkout_ga_code('begin_checkout');
}
add_action( 'woocommerce_after_checkout_form', 'pq_ga_checkout_progress' );
function pq_ga_checkout_progress(){
  pq_set_checkout_ga_code('checkout_progress');
}

function pq_set_checkout_ga_code($event){
  global $woocommerce;
  $items = $woocommerce->cart->get_cart();
  $cart = array();
  foreach($items as $item){
    $item_data = $item['data'];
    
    $categories = array();
    $terms =  get_the_terms($item['product_id'], 'product_cat');
    foreach($terms as $term){
      $categories []= $term->name;
    }
    $_item = array(
      'id' => $item_data->get_sku() ? $item_data->get_sku() : ( '#' . $item['id'] ),
      'name' => $item_data->get_name(),
      'brand' => implode(', ', $categories),
      'category' => $item['variation']['attribute_tamano-de-maceta'] ? $item['variation']['attribute_tamano-de-maceta'] : '',
      'variant' => $item['variation']['attribute_color-de-maceta'] ? $item['variation']['attribute_color-de-maceta'] : '',
      'quantity' =>  $item['quantity'],
      'price' => $item['line_total']
    );
    $cart[] = $_item;
  }
  $ga_code = "gtag( 'event', '".$event."',  {";
  $ga_code .= $event == 'checkout_progress' ? "'checkout_step': 2," : '';
  $ga_code .= "'items':" . json_encode($cart);
  $ga_code .= "} );";
  echo '<script>'.$ga_code.'</script>';

}

// //Vista de detalle de producto
add_action( 'woocommerce_after_single_product', 'pq_ga_product_detail' );
function pq_ga_product_detail(){
  global $product;
  if ( empty( $product ) ) {
    return;
  }
  $categories = array();
  $terms =  get_the_terms($product->get_id(), 'product_cat');
  foreach($terms as $term){
    $categories []= $term->name;
  }

  wc_enqueue_js( "
    gtag( 'event', 'view_item', {
      'id': '" . esc_js( $product->get_sku() ? $product->get_sku() : ( '#' . $product->get_id() ) ) . "',
      'name': '" . esc_js( $product->get_title() ) . "',
      'brand': '" . esc_js(implode(', ', $categories)) . "',
      'price': '" . esc_js( $product->get_price() ) . "',
    } );" );
}