<?php

//Show newsletter popup
add_action('wp_footer','pq_newsletter_popup');
function pq_newsletter_popup(){
  if(!is_front_page() || (isset($_COOKIE['newsletter_popup']) && $_COOKIE['newsletter_popup'] == 'hide')) return;
  echo do_shortcode('
  [lightbox auto_open="true" auto_timer="3000" auto_show="always" id="newsletter-signup-link" width="600px" padding="20px"]
  
  <h3>Suscríbete a nuestro Newsletter y obtén un <strong>10% de descuento</strong>.</h3>
  [contact-form-7 id="4671" title="Newsletter promo"]
  <div style="text-align: right"><a href="#" class="pq-newsletter-popup-not-show"><small>No volver a mostrar</small></a></div>
  [/lightbox]

  ');
}

//newsletter popup cookie
function pq_newsletter_popup_cookie(){
  // Check for nonce security
  $nonce = sanitize_text_field( $_POST['nonce'] );

  if ( ! wp_verify_nonce( $nonce, 'pq-nonce' ) ) {
      die ( 'Busted!');
  }

  $popupStatus = $_POST['popupStatus'];
  //Use a timespan of one year
  $remembering_timespan = $popupStatus == 'show' ? time() - 3600 : time() + 365 * 24 * 60 * 60;
  setcookie('newsletter_popup', $popupStatus, $remembering_timespan, '/', $_SERVER['HTTP_HOST']);
  wp_die($popupStatus);
}
add_action('wp_ajax_pq_newsletter_popup_cookie', 'pq_newsletter_popup_cookie');
add_action('wp_ajax_nopriv_pq_newsletter_popup_cookie', 'pq_newsletter_popup_cookie');

//validate existing email
add_filter( 'wpcf7_validate_email*', 'pq_cf7_validate_newsletter_email' , 10, 2 );
function pq_cf7_validate_newsletter_email( $result, $tag ) {
  $form_id = 4671;
  if ( isset($_POST['_wpcf7']) && $_POST['_wpcf7'] != $form_id)
    return $result;
  
  if ( !in_array( 'advanced-cf7-db/advanced-cf7-db.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) )
    return $result;
  
  global $wpdb;
  $type = $tag['type'];
  $name = $tag['name'];
  $value = $_POST[$name] ;

  $search = sanitize_text_field($value);
  $query = "SELECT * FROM `".VSZ_CF7_DATA_ENTRY_TABLE_NAME."` WHERE `cf7_id` = ".$form_id. " AND `name`= '".$name."' AND `value` LIKE '%%".$search."%%'";
  
  $data = $wpdb->get_results($query);
  if(!empty($data)){
    $result->invalidate( $tag, wpcf7_get_message( 'already_in_newsletter' ) );
  }
  return $result;
}

add_filter( 'wpcf7_messages', 'pq_cf7_newsletter_email_message' );
function pq_cf7_newsletter_email_message( $messages ) {
  return array_merge( $messages, array(
      'already_in_newsletter' => array(
          'description' => __( "Correo electrónico en newsletter", 'contact-form-7' ),
          'default' => __( 'Este correo electrónico ya se encuentra en nuestro newsletter', 'contact-form-7' )
      )
  ));
}

//Wc email styling shortcodes for use in plugins
add_shortcode('wc_email_header', 'pq_set_wc_email_header');
function pq_set_wc_email_header( $atts, $content = null ) {
  $a = shortcode_atts( array(
      'heading' => 'Haz recibido un descuento especial en Plantaqi',
  ), $atts );


  ob_start();
	wc_get_template( 'emails/email-header.php', array( 'email_heading' => esc_attr($a['heading']) ) );
  echo '<style>#template_header_image_table img{max-width: 200px;}';
  wc_get_template( 'emails/email-styles.php');
  echo '</style>';
  return ob_get_clean();
}

add_shortcode('wc_email_footer', 'pq_set_wc_email_footer');
function pq_set_wc_email_footer( $atts, $content = null ) {
  ob_start();
	wc_get_template( 'emails/email-footer.php');
  return ob_get_clean();
}

add_filter( 'wpcf7_special_mail_tags', 'pq_set_cf7_mail_tags_shortcode', 10, 3 );
function pq_set_cf7_mail_tags_shortcode( $output, $name, $html ) {
  if ( 'wc_email_footer' == $name || 'wc_email_header' == $name)
    $output = do_shortcode( "[".$name."]" );
  return $output;
}