<?php

//* Oculta resto de métodos de envío cuando envío gratuito está disponible
add_filter('woocommerce_package_rates', 'pq_hide_shipping_when_free_is_available', 100);
function pq_hide_shipping_when_free_is_available($rates)
{
	$free = array();
	foreach ($rates as $rate_id => $rate) {
		if ('free_shipping' === $rate->method_id) {
			$free[$rate_id] = $rate;
			break;
		}
	}
	return !empty($free) ? $free : $rates;
}

/**
 * Validar teléfono a 10 dígitos
 */

add_filter( 'woocommerce_checkout_fields', 'pq_remove_default_phone_validation' );
function pq_remove_default_phone_validation( $fields ){
    unset( $fields['billing']['billing_phone']['validate'] );
	return $fields;
}
add_filter( 'woocommerce_billing_fields' , 'custom_override_checkout_fields' );
function custom_override_checkout_fields( $fields ) {
    $fields['billing_phone']['maxlength'] = 10;    
    return $fields;
}
add_action('woocommerce_checkout_process', 'custom_validate_billing_phone');
function custom_validate_billing_phone() {
    $is_correct = preg_match('/^[0-9]{10}$/', $_POST['billing_phone']);
    if ( $_POST['billing_phone'] && !$is_correct) {
        wc_add_notice( __( 'El número de teléfono tiene que ser <strong>de 10 dígitos</strong>.' ), 'error' );
    }
}

add_action( 'woocommerce_after_add_to_cart_button', 'pq_after_add_to_cart_checkout_button' );
function pq_after_add_to_cart_checkout_button() {
	?>
	<style>.woocommerce-variation-add-to-cart{text-align: center;} </style>
	<a href="<?php echo esc_url( wc_get_checkout_url() ); ?>" class="checkout-button button alt wc-forward quick-checkout disabled">
		<?php esc_html_e( 'Proceed to checkout', 'woocommerce' ); ?>
	</a>
	<script>
	(function ($) {
			var $quickCheckout = $('.quick-checkout');
			var $addButton = $('.single_add_to_cart_button ');
			var product_id = $('.variations_form.cart').attr('data-product_id');

			$(document).on('change', 'table.variations select', function(){
				if(!$addButton.hasClass('disabled')){
					$quickCheckout.removeClass('disabled');
				}else{
					$quickCheckout.addClass('disabled');
				}
			});

			$(document).on('click', '.quick-checkout', function(e){
				e.preventDefault();
				if(!$addButton.hasClass('disabled')){
					var urlParams = '?add-to-cart='+product_id+'&variation_id='+$('input[name="variation_id"]').val();
					$('table.variations select').each(function(index, elem){
						urlParams += '&'+$(this).attr('name') + '=' + $(this).val();
					});
					window.location = $quickCheckout.attr('href')+urlParams;;
				}else{
					$('.single_add_to_cart_button').trigger('click');
				}
			});
	})(jQuery);
	</script>
	<?php
}

add_action('woocommerce_add_to_cart', 'pq_open_mini_cart_on_add');
function pq_open_mini_cart_on_add() {
	wc_enqueue_js( "
	if($('.header-cart-link.off-canvas-toggle').length) $('.header-cart-link.off-canvas-toggle').trigger('click');
	");
}

add_action( 'woocommerce_before_variations_form', 'pq_variation_info'); 
function pq_variation_info() { 
	echo "Cambia el color de tu maceta en el selector y mira cómo se vería tu color favorito en tu hogar.";
}

/**
 * Sobreescribir función de header para conservar el mismo en categorías
 */

function flatsome_category_header() {
	global $wp_query;

	// Set Custom Shop Header.
	if ( get_theme_mod( 'html_shop_page' ) && (is_shop() || is_product_category() ) && ! $wp_query->is_search() && $wp_query->query_vars['paged'] < 1 ) {
		echo do_shortcode( '<div class="custom-page-title">' . get_theme_mod( 'html_shop_page' ) . '</div>' );
		wc_get_template_part( 'layouts/headers/category-title' );
	} // Set Category headers.
	elseif ( is_shop() || is_product_tag() || is_product_taxonomy() ) {
		// Get Custom Header Content.
		$cat_header_style = get_theme_mod( 'category_title_style' );

		// Fix Transparent header.
		if ( get_theme_mod( 'category_header_transparent', 0 ) && ! $cat_header_style ) {
			$cat_header_style = 'featured';
		}

		$queried_object = get_queried_object();
		if ( ! is_shop() && get_term_meta( $queried_object->term_id, 'cat_meta' ) ) {
			$content = get_term_meta( $queried_object->term_id, 'cat_meta' );
			if ( ! empty( $content[0]['cat_header'] ) ) {
				if ( ! $cat_header_style ) {
					echo do_shortcode( $content[0]['cat_header'] );
					wc_get_template_part( 'layouts/headers/category-title' );
				} else {
					wc_get_template_part( 'layouts/headers/category-title', $cat_header_style );
					echo '<div class="custom-category-header">' . do_shortcode( $content[0]['cat_header'] ) . '</div>';
				}
			} else {
				// Get default header title.
				wc_get_template_part( 'layouts/headers/category-title', $cat_header_style );
			}
		} else {
			// Get default header title.
			wc_get_template_part( 'layouts/headers/category-title', $cat_header_style );
		}
	}
}

// shortcode para slider cross sells
add_shortcode("ux_slider_crosssells", "shortcode_ux_slider_crosssells");
function shortcode_ux_slider_crosssells(){
	global $product;
	$cross_sell_ids = $product->get_cross_sell_ids();
	if(!empty($cross_sell_ids)){
		return do_shortcode('[ux_products ids="'.implode(',', $cross_sell_ids).'" slider_bullets="true"]');
	}else{
		$terms = get_the_terms( $product->get_id(), 'product_cat' );
		foreach ($terms as $term) {
			$product_cat_id = $term->term_id;
			break;
		}
		return do_shortcode('[ux_products cat="'.$product_cat_id.'" slider_bullets="true" orderby="sales"]');
	}
}