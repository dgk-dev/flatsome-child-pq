<?php
// Analytics
require(get_stylesheet_directory().'/includes/analytics.php');

// Store changes
require(get_stylesheet_directory().'/includes/store-changes.php');

// Woocommerce plant data
require(get_stylesheet_directory().'/includes/plant-data.php');

// Newsletter handler
require(get_stylesheet_directory().'/includes/newsletter.php');

add_filter('wccf_datetimepicker_date_config', 'wccf_datetimepicker_datetime_config_change');
function wccf_datetimepicker_datetime_config_change($config) {
  $new_config = array_merge($config, array(
    'minDate' => '+03/01/1970',
    'disabledDates' => '["1/1/2021"]',
  ));
  return $new_config;
}

function pq_resources(){
  wp_enqueue_style('flatsome-child', get_stylesheet_directory_uri() . '/style.css', array('flatsome-main'), '1.0');
  wp_register_script( 'plantaqi-functions', get_stylesheet_directory_uri() . '/js/footer-bundle.js', null, '1.0', true);
  wp_enqueue_script( 'plantaqi-functions' );
  wp_localize_script('plantaqi-functions', 'plantaqiGlobalObject', array(
    'stylesheet_url' => get_stylesheet_directory_uri(),
    'ajax_url' => admin_url('admin-ajax.php'),
    'nonce'  => wp_create_nonce( 'pq-nonce' ),
  ));
}
add_action('wp_enqueue_scripts', 'pq_resources');

//Whatsapp
add_action('wp_footer','pq_add_footer_whatsapp');
function pq_add_footer_whatsapp(){
	$tel = "525526899083";

	$url = "https://wa.me/${tel}";
	$img = get_stylesheet_directory_uri().'/img/whatsapp-icon.svg';
	echo "<div id='float-whatsapp'>";
	echo "<a href=${url} target='_blank'>";
	echo "<img id='float-whatsapp-img' src='${img}' alt='whatsapp-icon' />";
	echo " </a>";
	echo "</div>";
}