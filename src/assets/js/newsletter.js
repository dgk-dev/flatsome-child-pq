(function ($) {
    document.addEventListener('wpcf7mailsent', function (event) {
        if(event.detail.contactFormId == '4671') {
            setNewsletterPopupCookie('hide');
        }
    }, false);

    function setNewsletterPopupCookie(popupStatus) {
        $.ajax({
            type: 'post',
            url: plantaqiGlobalObject.ajax_url,
            data: {
                action: 'pq_newsletter_popup_cookie',
                popupStatus: popupStatus,
                nonce: plantaqiGlobalObject.nonce
            },
            success: function (response) {
                console.log(response);
            },
            error: function (response) {
                console.log('error', response);
            },
        });
    }
})(jQuery);