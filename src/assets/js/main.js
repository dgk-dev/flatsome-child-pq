(function ($) {
    $(window).load(function(){
        var $invoice = $('#billing_invoice');
        if($invoice.length){
            var $customAddress = $('#wccf_checkout_field_direccion_env');
            var $customName = $('#wccf_checkout_field_de_regalo');
            var $customEmail = $('#wccf_checkout_field_email_usuario');
            var $customPhone = $('#wccf_checkout_field_telefono_envia');

            var $name = $('#billing_first_name');
            var $email = $('#billing_email');
            var $address = $('#billing_address_1');
            var $city = $('#billing_city');
            var $zip = $('#billing_postcode');
            var $phone = $('#billing_phone');
    
            var name = $name.val();
            var email = $email.val();
            var address = $address.val();
            var city = $city.val();
            var zip = $zip.val();
            var phone = $phone.val();

            $customPhone.attr('maxlength', 10);
            $phone.attr('maxlength', 10);
            
            $invoice.change(function(){
                $name.attr('value', (name ? name : $customName.val()));
                $email.attr('value', (email ? email :$customEmail.val()));
                $city.attr('value', ('CDMX'));
                $address.attr('value', (address ? address : $customAddress.val()));
                $phone.attr('value', (phone ? phone : $customPhone.val()));
                $zip.attr('value', (zip ? zip : '50000'));
            }).change();

            $customAddress.keyup(function(){
                if(!$invoice.prop('checked')){
                    var $this = $(this);
                    $address.attr('value', $this.val());
                }
            });
            $customName.keyup(function(){
                if(!$invoice.prop('checked')){
                    var $this = $(this);
                    $name.attr('value', $this.val());
                }
            });
            $customEmail.keyup(function(){
                if(!$invoice.prop('checked')){
                    var $this = $(this);
                    $email.attr('value', $this.val());
                }
            });
            $customPhone.keyup(function(){
                if(!$invoice.prop('checked')){
                    var $this = $(this);
                    $phone.attr('value', $this.val());
                }
            });
        }
    });
})(jQuery);