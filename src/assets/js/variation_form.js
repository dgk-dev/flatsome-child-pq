(function ($) {
    var $variations_form = $( ".variations_form" );
    var VariationForm;
	
	$variations_form.on( "wc_variation_form", function (e, variationForm) {
		VariationForm = variationForm;
	});
	
	$variations_form.on( "woocommerce_variation_has_changed", function () {
		if(!VariationForm) return;
		var chosenAttributes = VariationForm.getChosenAttributes();
		var variations = VariationForm.findMatchingVariations( VariationForm.variationData, chosenAttributes.data );
		if(variations[0]){
			VariationForm.$form.wc_variations_image_update(variations[0]);	
		}
	});
})(jQuery);